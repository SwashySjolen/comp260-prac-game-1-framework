﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	public Vector2 heading = Vector3.right; 
	public Transform player1;
	public Transform player2;

	// Update is called once per frame
	void Update ()
	{
		Vector2 direction1 = player1.position - transform.position;
		Vector2 direction2 = player2.position - transform.position;

		Vector2 direction;
		if (direction1.magnitude < direction2.magnitude) {
			direction = player1.position - transform.position;
		} else {
			direction = player2.position - transform.position;
		}
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft (heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate (angle);
		} else {
			// target on right, rotate clockwise
			heading = heading.Rotate (-angle);
		}

		transform.Translate (heading * speed * Time.deltaTime);
	}
	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}
}